export const ADD_EDGE = 'ADD_EDGE';
export const REMOVE_EDGE = 'REMOVE_EDGE';

export function addEdge(sourceNodeId,sourceConnector,targetNodeId,targetConnector){
    return{
        type:ADD_EDGE,
        id:sourceNodeId+"_"+targetNodeId,
        sourceNodeId,
        sourceConnector,
        targetNodeId,
        targetConnector
    }
}

export function removeEdge(edgeId){
    return{
        type:REMOVE_EDGE,
        edgeId
    }
}
