export const ADD_NODE = 'ADD_NODE';
export const REMOVE_NODE = 'REMOVE_NODE';

export function addNode(node,id,x,y) {
    return {
        type: ADD_NODE,
        node,
        id:id,
        x,
        y
    }
}

export function removeNode(id) {
    return {
        type: REMOVE_NODE,
        id
    }
}