import Konva from "konva"
import {addNode,removeNode} from "./Actions/nodeActions.js"
import {addEdge,removeEdge} from "./Actions/edgeActions.js"


export default class DrawingBord {
    constructor(container, width, height, store) {
        this._nodeLayer = new Konva.Layer({name: "NodeLayer"});
        this.lineLayer = new Konva.Layer({name: "LineLayer"});
        this.tempLineLayer = new Konva.Layer({name: "TempLineLayer"});
        this.lineStart = this.lineEnd = {};
        this.startShape = this.endShape = undefined;
        this.stage = new Konva.Stage({container, width, height, dw: this});
        this.stage.on("contentMousemove", this._mouseMove);
        this.stage.on("contentMouseup", this._mouseUp);
        this.stage.add(this.tempLineLayer, this.lineLayer, this._nodeLayer);
        this.findOne = (id)=>this.stage.find(id)[0];
        if (store) {
            this._store = store;
            this._store.subscribe(()=>this._stateChanged())
        }
    }

    _stateChanged() {
        var state = this._store.getState();
        var nodesLeft = state.nodes;

        this._nodeLayer.getChildren().each(function(n){
            let node = state.nodes.get(n.attrs.id);
            nodesLeft = nodesLeft.delete(n.attrs.id);
            if (node) {
                if (node.attrs.x !== n.x)
                    node.attrs.x = n.x;
                if (node.attrs.y !== n.y)
                    node.attrs.y = n.y;
            }else{
                n.destroy();
            }
        });
        nodesLeft.toKeyedSeq().forEach((n,k)=>{
            this.createNode(n.x,n.y,n.node,k)
        });
        this._nodeLayer.draw();
    }

    _mouseMove() {
        if (this.attrs.dw.isDrawingLine) {
            this.attrs.dw.lineEnd = this.getPointerPosition();
            this.attrs.dw._drawCurves();
        }
    }

    _mouseUp() {
        this.attrs.dw.isDrawingLine = false;
        this.attrs.dw.tempLineLayer.draw();
    }

    createNode(x, y, node, id) {
        var width = 100, nodeBaseHeight = 100, radius = 8;
        var inputs = Object.keys(node.inputs);
        var outputs = Object.keys(node.outputs);
        var spacing = 20 + radius;
        var height = Math.max(nodeBaseHeight, outputs.length * (spacing + radius), inputs.length * (spacing + radius));
        var nodeGroup = new Konva.Group({name: node.name, id: id, x, y, draggable: true, dw: this});

        if (node.name) {
            nodeGroup.add(new Konva.Text({y: 2, width, text: node.name, align: "center", fontSize: 12}));
        }

        nodeGroup.on("dragmove", ()=>this.lineLayer.draw());
        nodeGroup.add(new Konva.Rect({id: id + "_" + "rect", width, height, stroke: 'black', strokeWidth: 2}));

        this._buildConnector(radius / 4, id, "in", inputs, height, radius, spacing, nodeGroup);
        this._buildConnector(width, id, "out", outputs, height, radius, spacing, nodeGroup);

        this._nodeLayer.add(nodeGroup);
        this._nodeLayer.draw();
    }

    _buildConnector(xStart, id, conPrefix, connectors, totalHeight, radius, conSpacing, nodeGroup) {
        for (var i = 0; i < connectors.length; i++) {
            var circle = new Konva.Circle({
                id: id + "_" + conPrefix + "_" + connectors[i],
                x: xStart,
                y: totalHeight / (connectors.length + 1) + i * conSpacing,
                radius: radius,
                fill: 'blue',
                dw: this
            });

            circle.on("mousedown", this._connectorMouseDown);
            circle.on("mouseup", this._connectorMouseUp);
            nodeGroup.add(circle)
        }
    }

    _connectorMouseUp(ev) {
        this.attrs.dw.endShape = ev.target;
        this.attrs.dw.addLine(this.attrs.dw.startShape, this.attrs.dw.endShape, 'black', 2);
        this.attrs.dw.startShape = this.attrs.dw.endShape = undefined;
        ev.cancelBubble = true;
    }

    _connectorMouseDown(ev) {
        this.attrs.dw.startShape = ev.target;
        this.attrs.dw.isDrawingLine = true;
        this.attrs.dw.lineStart = this.attrs.dw.stage.getPointerPosition();
        ev.cancelBubble = true;
    }

    _drawCurves() {
        var c = this.tempLineLayer.getContext();
        c.clear();
        if (this.isDrawingLine) {
            c.beginPath();
            c.moveTo(this.lineStart.x, this.lineStart.y);
            c.lineTo(this.lineEnd.x, this.lineEnd.y);
            c.stroke();
        }
    }

    addLine(conA, conB, stroke, strokeWidth) {
        var lineShape = new Konva.Shape({
            id: conA.attrs.id + "->" + conB.attrs.id,
            conA, conB, stroke, strokeWidth,
            sceneFunc: function (context) {
                var start = this.attrs.conA.getAbsolutePosition();
                var end = this.attrs.conB.getAbsolutePosition();
                var controlX = start.x + (end.x - start.x) / 2;
                var controlY = end.y + (end.y - start.y) / 2;
                context.beginPath();
                context.moveTo(start.x, start.y);
                context.quadraticCurveTo(controlX, controlY, end.x, end.y);
                context.stroke();
            }
        });

        this.lineLayer.add(lineShape);
        this.tempLineLayer.draw();
        this.lineLayer.draw();
    }


}