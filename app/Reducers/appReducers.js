import { ADD_NODE,REMOVE_NODE } from '../Actions/nodeActions.js'
import { ADD_EDGE,REMOVE_EDGE } from '../Actions/edgeActions.js'
import { combineReducers } from 'redux'
import Immutable from 'immutable';
const defaultNodes = Immutable.OrderedMap();

function nodes(state = defaultNodes, action) {
    switch (action.type) {
        case ADD_NODE:
            let n = {
                x: action.x,
                y: action.y,
                node: action.node
            };
            return state.set(action.id, n);

        case REMOVE_NODE:
            return state.delete(action.id);
        default:
            return state
    }
}

const defaultEdges = Immutable.OrderedMap();

function edges(state = defaultEdges, action) {
    switch (action.type) {
        case REMOVE_NODE:
            return  state.filterNot((e)=>e.sourceNode === action.id || e.targetNode === action.id);
        case ADD_EDGE:
            let e = {
                sourceNode: action.sourceNodeId,
                sourceConnector: action.sourceConnector,
                targetNode: action.targetNodeId,
                targetConnector: action.targetConnector
            };
            return state.set(action.id,e);

        case REMOVE_EDGE:
            return state.delete(action.id);
        default:
            return state;
    }
}

const appReducer = combineReducers({
    nodes,
    edges
});

export default appReducer