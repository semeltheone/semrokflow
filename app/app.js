import DrawingBoard from "./DrawingBoard.js"

var dw = new DrawingBoard("canvas",2000,2000);


var conNode = {
    id: "a",
    name: "Konstante",
    inputs: {},
    outputs: {"value": 1},
    execute: function () {
        return {value:5};
    }
};

var addNode = {
    id: "add",
    name: "Add",
    inputs: {val1:1,val2:1},
    outputs: {"value": 1},
    execute: function (input) {
        return {value:input.val1+input.val2};
    }
};

