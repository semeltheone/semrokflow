import chai from 'chai';
var should = chai.should();
import DrawingBoard from "./../app/DrawingBoard.js"
import appReducer from "../app/Reducers/appReducers.js"
import {addNode,removeNode} from "../app/Actions/nodeActions.js"
import {createStore} from "redux"
import {generateId} from "../app/utils.js"

export default describe('Drawing Board store integration', function () {
    var dw;
    var store;
    var conNode = {
        name: "Konstante",
        inputs: {},
        outputs: {"value": 1},
        execute: function () {
            return {value: 5};
        }
    };

    beforeEach(function () {
        store = createStore(appReducer);
        var element = document.createElement("div");
        element.setAttribute("id", "canvas");
        dw = new DrawingBoard(element, 2000, 2000, store);
    });

    let dispatchAddNode = ()=> {
        let id = generateId();
        store.dispatch(addNode(conNode,id, 100, 100));
        return id;
    };

    it("should add node to the drawing board when dispatching the add action", ()=> {
        let node = dw.findOne("#" + dispatchAddNode());
        should.exist(node);
    });

    it("should only add one node when dispatching add", ()=> {
        let id = "#" + dispatchAddNode();
        dw.stage.find(id).length.should.equal(1);
    });

    it("should delete node from DrawingBoard when dispatching deleteNode action", ()=> {
        let x =  dispatchAddNode();

        store.dispatch( removeNode(x));
        let node = dw.findOne("#" + x);
        should.not.exist(node);
    });

})