import chai from 'chai';
var should = chai.should();
import DrawingBoard from "./../app/DrawingBoard.js"

export default describe('Drawing Board', function () {
    var dw;
    var conNode = {
        id: "a",
        name: "Konstante",
        inputs: {},
        outputs: {"value": 1},
        execute: function () {
            return {value: 5};
        }
    };
    var logNode = {
        id: "b",
        name: "Log",
        inputs: {"value": 1},
        outputs: {},
        execute: function (val) {
            console.log(val);
        }
    };
    beforeEach(function () {
        var element = document.createElement("div");
        element.setAttribute("id", "canvas");
        dw = new DrawingBoard(element, 2000, 2000);
    });

    describe('AddingNode', ()=> {
        it('should be findable by id', function () {
            dw.createNode(100, 100, conNode, conNode.id);
            var node = dw.findOne("#a");
            should.exist(node);
        });

        it("should create findable output connectors", ()=> {
            dw.createNode(100, 100, conNode, conNode.id);
            var connector = dw.findOne("#a_out_value");
            should.exist(connector);
        });

        it("should create findable input connectors", ()=> {
            dw.createNode(100, 100, logNode, logNode.id);
            var logConnector = dw.findOne("#b_in_value");
            should.exist(logConnector);
        });

        it("should create findable node rectangle", ()=> {
            dw.createNode(100, 100, logNode, logNode.id);
            const nodeRect = dw.findOne("#b_rect");
            should.exist(nodeRect);
        });
    });

    describe("Node Size", function () {
        it("should be 100x100 by default", ()=> {
            dw.createNode(100, 100, logNode, logNode.id);
            const nodeRect = dw.findOne("#b_rect");
            nodeRect.attrs.width.should.equal(100);
            nodeRect.attrs.height.should.equal(100);
        });

        it("should grow the height of the node to fit connectors", ()=> {
            var testNode = {
                id: "test", name: "Test",
                inputs: {"value": 1, "value2": 2, "value3": 2},
                outputs: {},
                execute: function (val) {
                }
            };
            dw.createNode(100, 100, testNode,testNode.id);
            var nodeRect = dw.findOne("#test_rect");
            nodeRect.attrs.height.should.be.above(100);
        });
    });

    describe("Connectors", ()=> {
        it("should be filled blue", ()=> {
            dw.createNode(100, 100, logNode,logNode.id);
            const connector = dw.findOne("#b_in_value");
            connector.attrs.fill.should.equal("blue")
        });
    })
});