import { createStore } from 'redux'
import appReducer from "../app/Reducers/appReducers.js"
import {addNode,removeNode} from "../app/Actions/nodeActions.js"
import {addEdge,removeEdge} from "../app/Actions/edgeActions.js"
import chai from 'chai';
var should = chai.should();
import {generateId} from "../app/utils.js"

export default describe("EdgeReducers", function () {
    var store;
    beforeEach(function () {
        store = createStore(appReducer);
    });

    let id =()=>generateId();

    it("should be able to add edge", ()=> {
        store.dispatch(addEdge("a","b","c","d"));
        store.getState().edges.size.should.be.above(0);
    });

    it("should initialize the edge with a source node", ()=> {
        store.dispatch(addEdge("a","b","c","d"));
        store.getState().edges.first().sourceNode.should.equal("a");
    });

    it("should initialize the edge with a target node", ()=> {
        store.dispatch(addEdge("a","b","c","d"));
        store.getState().edges.first().targetNode.should.equal("c");
    });

    it("should initialize the edge with a source node connector", ()=> {
        store.dispatch(addEdge("a","b","c","d"));
        store.getState().edges.first().sourceConnector.should.equal("b");
    });

    it("should initialize the edge with a target node connector", ()=> {
        store.dispatch(addEdge("a","b","c","d"));
        store.getState().edges.first().targetConnector.should.equal("d");
    });

    it("should remove edge when node is removed", ()=> {
        let n1 = "a";
        let n2 = "x";
        store.dispatch(addNode({},n1,10,10));
        store.dispatch(addNode({},n2,150,10));
        store.dispatch(addEdge(n1,"b",n2,"d"));
        store.getState().edges.size.should.equal(1);
        store.dispatch(removeNode("a"));
        store.getState().edges.size.should.equal(0);

    });
});