import { createStore } from 'redux'
import appReducer from "../app/Reducers/appReducers.js"
import {addNode} from "../app/Actions/nodeActions.js"
import chai from 'chai';
var should = chai.should();
import {generateId} from "../app/utils.js"

export default describe("NodeReducers", function () {
    var store;
    beforeEach(function () {
        store = createStore(appReducer);
    });
    let id=()=>generateId();
    var logNode = {
        id: "b",
        name: "Log",
        inputs: {"value": 1},
        outputs: {},
        execute: function (val) {
            console.log(val);
        }
    };

    it("should add one node to the state when calling add node", ()=> {
        store.dispatch(addNode(logNode,id(), 100, 100));
        store.getState().nodes.size.should.equal(1);
    });

    it("should add x and y props to the node when adding", ()=> {
        store.dispatch(addNode(logNode,id(), 100, 50));
        var addedNode = store.getState().nodes.last();
        addedNode.x.should.equal(100);
        addedNode.y.should.equal(50);
    });
});