module.exports = {
    devtool: 'source-map',
    entry: {
        main:"./app/app.js",
        test:"./tests/tests.js"
    },
    output: {
        path: __dirname,
        filename: "[name]bundle.js",
        sourceMapFilename:"[file].map"
    },
    module: {
        loaders: [
            {
                loader: "babel-loader",
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                query: {

                    presets: ['es2015', 'react'],
                }
            }
        ]
    }
};